<?php

class EjemploModificadores{

    public function metodo1(){
        echo "Este es el metodo 1";
    }

    private function metodo2(){
        echo "<br/>Este es el metodo 2";
    }

    protected function metodo3(){
        echo "<br/>Este es el metodo 3";
    }

    public function AccesoMetodo2(){
        $this->metodo2();
    }
}

class Hijo extends EjemploModificadores{
    public function AccesoMetodo3(){
        $this->metodo3();  
    }
}


$obj= new Hijo;
$obj->metodo1();
$obj->AccesoMetodo3();
$obj2= new EjemploModificadores;
$obj2->AccesoMetodo2();

?>