<?php

abstract class Transporte{

    abstract protected function Mantenimiento();
}

class Avion extends Transporte{

    public function Mantenimiento(){
        echo "AVION";
        echo "<br/>Revision de Aceites";
        echo "<br/>Revision de Combustible";
        echo "<br/>Revision de Ruedas";
        echo "<br/>Revision de Frenos";
        echo "<br/>Revision de Motores";
        echo "<br/>Revision de Estructuta";
    }
}

class Auto extends Transporte{

    public function Mantenimiento(){
        echo "<br/><br/>AUTO";
        echo "<br/>Revision de Motor";
        echo "<br/>Revision de Aceite";
        echo "<br/>Revision de Frenos";
        echo "<br/>Revision de Combustible";
        echo "<br/>Revision de Bateria";
        echo "<br/>Revision de Sistema de Iluminacion";
        echo "<br/>Revision de Neumaticos";
        echo "<br/>Revision de Simbolos en el Tablero";
    }
}

class Tren extends Transporte{

    public function Mantenimiento(){
        echo "<br/><br/>TREN";
        echo "<br/>Revision de Via";
        echo "<br/>Rehabilitacion Progresiva de via";
        echo "<br/>Sustitucion de Desvios";
        echo "<br/>Sustitucion de Carril";
        echo "<br/>Revision de Ruedas";
        echo "<br/>Revision de Frenos";
        echo "<br/>Revision de Motores";
        echo "<br/>Revision de Señales de seguridad";
        echo "<br/>Revision de Plataformas";
        echo "<br/>Revision de Pasos a nivel";
    }
}

class Barco extends Transporte{

    public function Mantenimiento(){
        echo "<br/><br/>BARCO";
        echo "<br/>Revision de Motores";
        echo "<br/>Revision de Combiustible";
        echo "<br/>Revision de Filtros";
        echo "<br/>Revision de Aceites";
        echo "<br/>Revision de Baterias";
        echo "<br/>Revision de Parametros";
        echo "<br/>Revision y reparacion de averias";
        echo "<br/>Revision de Engrase";
        echo "<br/>Revision de Limpieza";
        echo "<br/>Revision de Reglajes";
        echo "<br/>Revision de Controles";
        echo "<br/>Revision de piezas";
    }
}

class Bicicleta extends Transporte{

    public function Mantenimiento(){
        echo "<br/><br/>BICICLETA";
        echo "<br/>Revision de Cadena";
        echo "<br/>Revision de Platos";
        echo "<br/>Revision de Piñones";
        echo "<br/>Revision de Desviador trasero";
        echo "<br/>Revision de Llanta";
        echo "<br/>Revision de Cubierta";
        echo "<br/>Revision de palancas de frenos";
        echo "<br/>Revision de Ajuste de ruedas y asiento";
        echo "<br/>Revision de Lubricacion";
        echo "<br/>Revision de Limpieza";
        echo "<br/>Revision de Aire de las camaras";
    }
}

$obj = new Avion();
$obj->Mantenimiento();

$obj2 = new Auto();
$obj2->Mantenimiento();

$obj3 = new Tren();
$obj3->Mantenimiento();

$obj4 = new Barco();
$obj4->Mantenimiento();

$obj5 = new Bicicleta();
$obj5->Mantenimiento();

?>