<?php

abstract class Internet{

    abstract public function Altacalidad();
    abstract public function Bajacalidad();
}

class Megacable extends Internet{

    public function Bajacalidad(){
        echo "MEGACABLE";
        echo "<br/>Internet de baja velicidad";
        echo "<br/>Mala antencion al cliente";
        echo "<br/>Cableado de Cobre";
    }
}

class TotalPlay extends Internet{

    public function Altacalidad(){
        echo "<br/>TOTALPLAY";
        echo "<br/>Internet de alta velocidad";
        echo "<br/>Buenta atencion al cliente y respuesta rapida";
        echo "<br/>Cableado de Fibra Optica";
    }
}

$obj = new Megacable();
$obj->Bajacalidad();

$obj2 = new TotalPlay();
$obj2->Altacalidad();

?>