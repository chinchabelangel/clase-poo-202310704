<?php
 
 //Hacemos una clase
class Conversor{
    
    //Ponemos un atributo
    public $ConversionRealizar = "";
    
    //Nuestros metodos con parametro a
    public function Euro_a_Dolar($a){
        return $a * 1.178;
    }
    
    public function Euro_a_Libras($a){
        return $a * .87;
    }

    public function Euro_a_Pesos($a){
        return $a * 24.03;
    }

    public function Dolar_a_Euro($a){
        return $a * .83;
    }

    public function Dolar_a_Libras($a){
        return $a * .73;
    }

    public function Dolar_a_Pesos($a){
        return $a * 20.05;
    }

    public function Libras_a_Dolar($a){
        return $a * 1.38;
    }

    public function Libras_a_Euro($a){
        return $a * 1.15;
    }

    public function Libras_a_Pesos($a){
        return $a * 27.66;
    }

    public function Pesos_a_Dolar($a){
        return $a * .050;
    }

    public function Pesos_a_Euro($a){
        return $a * .042;
    }

    public function Pesos_a_Libras($a){
        return $a * .036;
    }

    //Metodo que ejecuta los metodos en base a la propiedad $ConversionRealizar y tiene un parametro a
    public function ResultadoConversion($a){
        switch ($this->ConversionRealizar) {
            case 'euro_dolar':
                return $this->Euro_a_Dolar($a);
                break;
            case 'euro_libras':
                return $this->Euro_a_Libras($a);
                break;
            case 'euro_pesos':
                return $this->Euro_a_Pesos($a);
                break;
            case 'dolar_euro':
                return $this->Dolar_a_Euro($a);
                break;
            case 'dolar_libras':
                return $this->Dolar_a_Libras($a);
                break;
            case 'dolar_pesos':
                return $this->Dolar_a_Pesos($a);
                break;
            case 'libras_dolar':
                return $this->Libras_a_Dolar($a);
                break;
            case 'libras_euro':
                return $this->Libras_a_Euro($a);
                break;
            case 'libras_pesos':
                return $this->Libras_a_Pesos($a);
                break;
            case 'pesos_dolar':
                return $this->Pesos_a_Dolar($a);
                break;
            case 'pesos_euro':
                return $this->Pesos_a_Euro($a);
                break;
            case 'pesos_libras':
                return $this->Pesos_a_Libras($a);
                break;    
            default:
                return 'No se puede realizar la Conversion';
                break;
        }
    }

}

?>