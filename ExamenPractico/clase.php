<?php

class Comida{

    private $PrecioComida;

    function __construct($PrecioComida){
        $this->PrecioComida = $PrecioComida;
    }

    public function Burrito($a){
        return $a * 12;
    }

    public function Gordita($a){
        return $a * 10;
    }

    public function Omelet($a){
        return $a * 20;
    }

    public function Chilaquiles($a){
        return $a * 35;
    }

    public function Pozole($a){
        return $a * 45;
    }

    public function OrdenFinal($a){
        switch ($this->PrecioComida) {
            case 'Burrito':
                return $this->Burrito($a);
                break;
            case 'Gordita':
                return $this->Gordita($a);
                break;
            case 'Omelet':
                return $this->Omelet($a);
                break;
            case 'Chilaquiles':
                return $this->Chilaquiles($a);
                break;
            case 'Pozole':
                return $this->Pozole($a);
                break;
        }    
           
    }
}

?>