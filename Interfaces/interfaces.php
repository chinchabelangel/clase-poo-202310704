<?php

interface OperacionesGenerales{
    public function Redondeo($monto);
    public function CalcularIVA($monto);
}

class PuntoDeVenta implements OperacionesGenerales{
    public function Redondeo($monto){
        echo round($monto);
    }

    public function CalcularIVA($monto){
        echo $IVA=$monto*.16;
    }
}

?>