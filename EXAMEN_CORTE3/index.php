<?php

class Examen{

    public $velocidad;

    protected function acelerar(){
        echo "<br/>Se esta acelerando";
    }

    private function frenar(){
        echo "<br/>Se esta empezando a frenar";

    }

    public function verVelocidad(){
        echo "<br/>La velocidad es de 100";
    }

    public function AccesoFrenar(){
        $this->frenar();
    }
   
}

class Aceleracion extends Examen{
    public function AccesoAcelerar(){
        $this->acelerar();
    }
}

$obj = new Aceleracion;
$obj->verVelocidad();
$obj->AccesoAcelerar();
$obj2 = new Examen;
$obj2->AccesoFrenar();

?>


