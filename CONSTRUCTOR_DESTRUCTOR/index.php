<?php

class Ejemplo{

    public $Atributo_Ejemplo;

    function __construct(){
        echo "Este es el Constructor";
        $this->Atributo_Ejemplo=23;
    }

    function __destruct(){
        echo "  Este es el Destructor";
        $this->Atributo_Ejemplo=null;
    }

}

$obj=new Ejemplo();
echo $obj->Atributo_Ejemplo;

?>