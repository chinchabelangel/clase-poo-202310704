<?php

class Auto{

    protected $color;
    protected $modelo;
    protected $velocidad;

    public function __construct($color, $modelo, $velocidad = 0){
        $this->modelo = $modelo;
        $this->color = $color;
        $this->velocidad = $velocidad;
    }

    private function obtenerColor(){
        return $this->color;
    }

    private function darColor($color){
        $this->color=$color;
    }

    private function acelerar(){
        $this->velocidad++;
    }

    private function frenar(){
        $this->velocidad--;
    }

    private function obtenerVelocidad(){
        return $this->velocidad;
    }

    public function mostrarInfo(){
        $info = "<h1>Información del coche:</h1>";
        $info.= "Modelo: ".$this->modelo;
        $info.= "<br/> Color: ".$this->obtenerColor();
        $info.= "<br/> Velocidad: ".$this->obtenerVelocidad();

        return $info;
    }
}

?>